from django.shortcuts import render
from projects.models import Project


def project_list(request):
    project = Project.objects.all()
    context = {
        'project_list': project,
    }
    return render(request, 'projects/list.html', context)
